import React, {Component} from 'react'
import * as THREE from 'three'

class App extends Component {

    constructor(props) {
        super(props);
        let bricksNum = 9;
        this.state = {
            clock: null,
            brick: [],
            minPosition: new THREE.Vector3(0, 1, 0),
            maxPosition: new THREE.Vector3(bricksNum - 1, -7, bricksNum - 1)
        };

        this.keyState = {};

        this.start = this.start.bind(this);
        this.stop = this.stop.bind(this);
        this.animate = this.animate.bind(this);
        this.init = this.init.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
        this.onKeyUp = this.onKeyUp.bind(this);
        this.onWheel = this.onWheel.bind(this);
        this.onMouseDown = this.onMouseDown.bind(this);
        this.onMouseMove = this.onMouseMove.bind(this);
        this.clearGrid = this.clearGrid.bind(this);
        this.initBricks = this.initBricks.bind(this);
    }

    clearGrid() {
        let grid = [],
            x,
            y,
            z;
        for (y = 0; y < 9; y++) {
            grid[y] = [];
            for (x = 0; x < 9; x++) {
                grid[y][x] = [];
                for (z = 0; z < 1; z++) {
                    grid[y][x][z] = 0;
                }
            }
        }
        this.grid = grid;
    }

    initBricks() {
        let b;
        for (b = 0; b < 10; b++) {
            let brick = {
                geometry: new THREE.BoxGeometry(1, 1, 1),
                material: new THREE.MeshBasicMaterial({color: this.generateColor()}),
                collision: true
            };
            this.state.brick.push(brick);
        }
    }

    generateColor() {
        return '#' + Math.random().toString(16).substr(-6);
    }

    componentDidMount() {
        this.width = this.mount.offsetWidth;
        this.height = this.mount.offsetHeight;
        this.clearGrid();
        this.initBricks();
        this.init();
    }

    init() {
        let aspect = this.width / this.height,
            d = 10,
            scene = new THREE.Scene(),
            camera = new THREE.OrthographicCamera(-d * aspect, d * aspect, d, -d, 1, 1000),
            renderer = new THREE.WebGLRenderer({antialias: true});
        this.setState({
            scene: scene,
            camera: camera,
            renderer: renderer
        });
        camera.position.set(20, 20, 20);
        // camera.position.set(90, 90, 90);
        camera.lookAt(scene.position);
        let x,
            y,
            z;

        for (z = 0; z < this.grid.length; z += 1) {
            for (x = 0; x < this.grid[z].length; x += 1) {
                for (y = 0; y < this.grid[z][x].length; y += 1) {
                    let brk = this.state.brick[this.grid[z][x][y]];
                    let brick = {
                        geometry: brk['geometry'],
                        material: brk['material'],
                        position: new THREE.Vector3(x, y, z)
                    };

                    let mesh = new THREE.Mesh(brick.geometry, brick.material);
                    scene.add(mesh);
                    mesh.position.set(brick.position.x, brick.position.y, brick.position.z);
                    this.state.brick.push(brick);
                }
            }
        }

        let brickHero = {
            // geometry: new THREE.SphereGeometry(1, 32, 32),
            geometry: new THREE.BoxGeometry(1, 1, 1),
            material: new THREE.MeshBasicMaterial({color: "#666666"}),
            position: new THREE.Vector3(0, 1, 0)
        };

        let hero = new THREE.Mesh(brickHero.geometry, brickHero.material);
        scene.add(hero);
        hero.position.set(brickHero.position.x, brickHero.position.y, brickHero.position.z);

        renderer.setClearColor('#f0f0f0');
        renderer.setSize(this.width, this.height);

        this.setState({
            scene: scene,
            camera: camera,
            renderer: renderer
        });

        this.hero = hero;
        this.mount.appendChild(renderer.domElement);
        this.start();
    }

    onKeyDown(e) {
        this.keyState[e.key] = true;
    }

    onKeyUp(e) {
        this.keyState[e.key] = false;
    }

    onWheel(e) {
        if (e.deltaY > 0) {
            let camera = this.state.camera;
            camera.zoom -= 0.1;
            if (camera.zoom <= 0.5) {
                camera.zoom = 0.5;
            }
            camera.updateProjectionMatrix();
            this.setState({
                camera: camera
            });
        } else {
            let camera = this.state.camera;
            camera.zoom += 0.1;
            if (camera.zoom >= 3) {
                camera.zoom = 3;
            }
            camera.updateProjectionMatrix();
            this.setState({
                camera: camera
            });
        }
    }

    onMouseDown(e) {
        if (!this.mouseDown)
            this.mouseDown = false;
        this.mouseDown = !this.mouseDown;
    }

    onMouseMove(e) {
        if (this.mouseDown) {
            // console.log(e);
        }
    }

    componentWillMount() {
        window.addEventListener('keydown', this.onKeyDown, true);
        window.addEventListener('keyup', this.onKeyUp, true);
        window.addEventListener('wheel', this.onWheel, true);
        window.addEventListener('mousedown', this.onMouseDown, true);
        window.addEventListener('mouseup', this.onMouseDown, true);
        window.addEventListener('mousemove', this.onMouseMove, true);
    }

    componentWillUnmount() {
        // this.stop();
        // this.mount.removeChild(this.state.renderer.domElement);
    }

    start() {
        let hero = this.hero;
        if (this.keyState[' ']) {
            if (!this.state.clock) {
                this.setState({
                    clock: Date.now()
                });
                hero.position.y += 1;
            }
        } else if (this.keyState['ArrowUp']) {
            if (this.state.minPosition.z < hero.position.z) {
                hero.position.z -= 1;
            }
        } else if (this.keyState['ArrowRight']) {
            if (this.state.maxPosition.x > hero.position.x) {
                hero.position.x += 1;
            }
        } else if (this.keyState['ArrowDown']) {
            if (this.state.maxPosition.z > hero.position.z) {
                hero.position.z += 1;
            }
        } else if (this.keyState['ArrowLeft']) {
            if (this.state.minPosition.x < hero.position.x) {
                hero.position.x -= 1;
            }
        }

        this.setState({
            hero: hero
        });
        if (this.state.renderer) {
            this.renderScene();
        }
        if (!this.frameId) {
            this.frameId = requestAnimationFrame(this.animate);
        }
        setTimeout(this.start, 10);
    }

    stop() {
        cancelAnimationFrame(this.frameId);
    }

    animate() {
        this.renderScene();
        if (this.state.clock && ((this.state.clock + 500) <= Date.now())) {
            let hero = this.hero;
            hero.position.y -= 1;
            this.setState({
                clock: null,
                hero: hero
            });
        }
        if (this.state.renderer) {
            this.renderScene();
        }
        this.frameId = window.requestAnimationFrame(this.animate);
    }

    renderScene() {
        this.state.renderer.render(this.state.scene, this.state.camera);
    }

    render() {
        return (
            <div
                style={{width: '700px', height: '600px'}}
                ref={(mount) => {
                    this.mount = mount
                }}
            />
        )
    }
}

export default App